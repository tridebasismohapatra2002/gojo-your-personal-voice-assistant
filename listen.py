import speech_recognition as sr
from speak import text_to_speech

recognizer = sr.Recognizer()

def listen_voice():
    with sr.Microphone() as source:
        print("Say something:")
        audio = recognizer.listen(source , phrase_time_limit = 10)

    try:
        text = recognizer.recognize_google(audio)
        print("You said:", text)
    except sr.UnknownValueError:
        text = "Could not understand the audio , please say it again"
        print("Gojo :" , text)
        text_to_speech(text)
        listen_voice()
    except sr.RequestError as msg:
        print("Error :", msg)
        text_to_speech('sorry request failed , please say it again')
        listen_voice()

    return text
