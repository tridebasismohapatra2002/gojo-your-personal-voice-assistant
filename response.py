import os
from langchain_community.llms import HuggingFaceEndpoint
from langchain_core.prompts import ChatPromptTemplate

HUGGINGFACEHUB_API_KEY = input('Enter The API Token:')

os.environ['HUGGINGFACEHUB_API_KEY'] = HUGGINGFACEHUB_API_KEY

model_id = 'mistralai/Mixtral-8x7B-Instruct-v0.1'

conv_model = HuggingFaceEndpoint(huggingfacehub_api_token = os.environ['HUGGINGFACEHUB_API_KEY'],
                           repo_id = model_id,
                           temperature = 0.6 , 
                           max_new_tokens = 700)

def generate_response(text):
    prompt = ChatPromptTemplate.from_messages([
        ('system' , """Your name is Gojo and you are a convesational chatbot used to provide response as per the query. please try to answer the queries politely and consisely and always say ,"thanks for asking , is there anything else in which I can help you" in the end."""),
        ('human' , '{input}'),
    ])

    chain = prompt | conv_model
    try:
        res = chain.invoke({'input' : text})
        res = res.split('Gojo: ')[-1]
        print('Gojo :' , res)
    except:
        res = chain.invoke({'input' : text})
        res = res.split('Gojo: ')[-1]
        print('Gojo :' , res)
    return res