from listen import listen_voice
from response import generate_response
from speak import text_to_speech

if __name__ == '__main__':
    initial_text = 'Hi my name is Gojo and I am your virtual assistant. With my vast knowledge I am here to help you. Feel free to ask me anything.'
    text_to_speech(initial_text)

ending_speeches = ['quit the session' , 'shutdown yourself' , 'terminate the session' , 'quit this session' , 'terminate this session' , 'shut down yourself']

while True:    
    query = listen_voice()
    if query in ending_speeches:
        break
    else:
        result = generate_response(query)
        text_to_speech(result)